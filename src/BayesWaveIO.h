/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif


/* ********************************************************************************** */
/*                                                                                    */
/*                        Data handling and injection routines                        */
/*                                                                                    */
/* ********************************************************************************** */

REAL8TimeSeries *readTseries(CHAR *cachefile, CHAR *channel, LIGOTimeGPS start, REAL8 length);

void InjectFromMDC(ProcessParamsTable *commandLine, LALInferenceIFOData *IFOdata, double *SNR);
void BayesWaveInjection(ProcessParamsTable *commandLine, struct Data *data, struct Chain *chain, struct Prior *prior, double **psd, int *NC);
void getChannels(ProcessParamsTable *commandLine, char **channel);

// reads command line and returns number of IFOs
int get_number_of_ifos_from_command_line(ProcessParamsTable *commandLine);
/*
 * sets list of ifos names from command line
 * Modifies: ifos
 * Returns: list of ifo names
 */
char ** get_list_of_ifo_names_from_command_line(ProcessParamsTable *commandLine);
/*
 * frees ifo list set by set_list_of_ifos_from_command_line
 */
void free_list_of_ifo_names(int NI, char** ifos);

/*
 * Modifies the command line arguments so that the --IFO-cache point to the checkpointed psds instead of cache files
 * Only will modify command line if checkpointing has happened (this avoids a transient frame read error)
 * Modifies
 *  commandLine
 * Returns 1 (True) if checkpoint file exists, returns 0 (False) if file does not exist
 */
int set_cache_from_checkpoint_asd(ProcessParamsTable *commandLine); 

/*
 * Modifies data->s and sets it to the value in checkpoint/data.dat.IFO_NUM file
*/
void set_frequency_data_from_checkpoint(struct Data *data);



/* ********************************************************************************** */
/*                                                                                    */
/*                                   Output Files                                     */
/*                                                                                    */
/* ********************************************************************************** */

void write_gnuplot_script_header(FILE *script, char modelname[]);
void write_gnuplot_script_frame (FILE *script, char modelname[], int signal, int glitch, int cycle, int NI);
void write_bayesline_gnuplot_script_frame(FILE *script, char modelname[], int cycle);
void write_bayeswave_gnuplot_script_frame(FILE *script, char modelname[], double Tobs, double fmin, double fmax, int phase, int signal, int glitch, int cycle, int NI);
void write_evidence_gnuplot_script(FILE *script, char modelname[]);
void write_gnuplot_psd_animation(struct Data *data, char modelname[], int frame);
void print_bayesline_spectrum(char filename[], double *f, double *power, double *Sbase, double *Sline, int N);

void print_version(FILE *fptr);
void print_run_stats(FILE *fptr, struct Data *data, struct Chain *chain);
void print_cbc_run_stats(FILE *fptr, struct Data *data, struct bayesCBC *bayescbc);
void print_run_flags(FILE *fptr, struct Data *data, struct Prior *prior);
void print_chain_files(struct Data *data, struct Chain *chain, struct Model **model, struct BayesLineParams ***bayesline, struct bayesCBC *bayescbc, int ic);
void print_chain_status(struct Data *data, struct Chain *chain, struct Model **model, int searchFlag);
void flush_chain_files(struct Data *data, struct Chain *chain, int ic);

void print_model(FILE *fptr, struct Data *data, struct Chain *chain, struct Model *model);
void print_signal_model(FILE *fptr, struct Model *model, int n);
void print_glitch_model(FILE *fptr, struct Wavelet *glitch);
void print_cbc_checkpoint_model(FILE *fptr, struct Model *model, int NX);
void print_cbc_chain_model(FILE *fptr, struct bayesCBC *bayescbc, int ic, double Tobs);
void print_cbc_history(FILE *fptr, struct bayesCBC *bayescbc, int ic);

void print_time_domain_waveforms(char filename[], double *h, int N, double *Snf, double Tobs, int imin, int imax, double tmin, double tmax);
void print_time_domain_hdot(char filename[], double *h, int N, double *Snf, double Tobs, int imin, int imax, double tmin, double tmax);
void print_frequency_domain_waveforms(char filename[], double *h, int N, double *Snf, double Tobs, int imin, int imax);
void print_frequency_domain_data(char filename[], double *h, int N, double Tobs, int imin, int imax);
void print_colored_time_domain_waveforms(char filename[], double *h, int N, double Tobs, int imin, int imax, double tmin, double tmax);

void parse_command_line(struct Data *data, struct Chain *chain, struct Prior *prior, ProcessParamsTable *commandLine);
void parse_glitch_parameters(struct Data *data, struct Model *model, FILE **paramsfile, double **grec);
void parse_signal_parameters(struct Data *data, struct Model *model, FILE **paramsfile, double **hrec);
void parse_bayesline_parameters(struct Data *data, struct Model *model, FILE **splinechain, FILE **linechain, double **psd);
void parse_cbc_parameters(struct Data *data, struct Model *model, struct Chain *chain, struct bayesCBC *bayescbc, FILE *paramsfile, int ic);
void parse_cbc_parameters_chain_model(struct Data *data, struct Model *model, struct Chain *chain, struct bayesCBC *bayescbc, FILE *paramsfile, int ic);
void parse_cbc_history(FILE *fptr, struct bayesCBC *bayescbc, int ic);
void dump_time_domain_waveform(struct Data *data, struct Model *model, struct Prior *prior, char root[]);

void export_cleaned_data(struct Data *data, struct Model *model);

void system_pause();

