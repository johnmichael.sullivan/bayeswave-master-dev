#define TSUN 4.92569043916e-6                                           // mass to seconds conversion
#define PI 3.1415926535897931159979634685442            // Pi
#define PI2 9.869604401089357992304940125905      // Pi^2
#define TPI 6.2831853071795862319959269370884     // 2 Pi
#define RT2PI 2.5066282746310005024               // sqrt(2 Pi)
#define RTPI 1.772453850905516                    // sqrt(Pi)
#define RT2 1.414213562373095                      // sqrt(2)
#define IRT2 0.707106781186547549                      // 1/sqrt(2)
#define TRT2 2.8284271247461901                  // 2*sqrt(2)
#define LN2 0.6931471805599453                 // ln 2

#define HLdt 0.0106                             // Maximum light travel time allowed H-L
#define HVdt 0.0278                             // Maximum light travel time allowed H-V
#define LVdt 0.0257                            // Maximum light travel time allowed L-V

#define dtmax 0.022                          // radius of Earth in seconds

#define Qs 16.0          // Q used in the glitch cleaning for spectral estimation

#define NCH 8        // number of chains
#define NS 7        // number of sky parameters
#define lhold 0     // fixed likelihood run when =1
